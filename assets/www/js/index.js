/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */


// This server should be updated
var serverUrl = "http://192.168.18.55:3001/";

var localStream, room, recording, recordingId;

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
      results = regex.exec(location.search);
  return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


function testConnection () {
  window.location = "/connection_test.html";
}

function startRecording () {
  if (room !== undefined){
    if (!recording){
      room.startRecording(localStream, function(id) {
        recording = true;
        recordingId = id;
      });
      
    } else {
      room.stopRecording(recordingId);
      recording = false;
    }
  }
}

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        /*
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
        */
      recording = false;
      var screen = getParameterByName("screen");
      //var config = {audio: true, video: true, data: true, screen: screen, videoSize: [640, 480, 640, 480]};
      //var config = {audio: true, video: false, data: false, screen: false};
      var config = {audio: true, video: false, data: false, screen: screen, videoSize: [640, 480, 640, 480]};
      // If we want screen sharing we have to put our Chrome extension id. The default one only works in our Lynckia test servers.
      // If we are not using chrome, the creation of the stream will fail regardless.
      if (screen){
        config.extensionId = "okeephmleflklcdebijnponpabbmmgeo";
      }
      localStream = Erizo.Stream(config);
      var createToken = function(userName, role, callback) {

        var req = new XMLHttpRequest();
        var url = serverUrl + 'createToken/';
        var body = {username: userName, role: role};

        req.onreadystatechange = function () {
          if (req.readyState === 4) {
            callback(req.responseText);
          }
        };

        req.open('POST', url, true);
        req.setRequestHeader('Content-Type', 'application/json');
        req.send(JSON.stringify(body));
      };

      createToken("user", "presenter", function (response) {
        var token = response;
        console.log(token);
        room = Erizo.Room({token: token});

        localStream.addEventListener("access-accepted", function () {
          var subscribeToStreams = function (streams) {
            for (var index in streams) {
              var stream = streams[index];
              if (localStream.getID() !== stream.getID()) {
                room.subscribe(stream);
              }
            }
          };

          room.addEventListener("room-connected", function (roomEvent) {

            room.publish(localStream, {maxVideoBW: 300});
            subscribeToStreams(roomEvent.streams);
          });

          room.addEventListener("stream-subscribed", function(streamEvent) {
            var stream = streamEvent.stream;
            var div = document.createElement('div');
            div.setAttribute("style", "width: 320px; height: 240px;");
            div.setAttribute("id", "test" + stream.getID());

            document.body.appendChild(div);
            stream.show("test" + stream.getID());

          });

          room.addEventListener("stream-added", function (streamEvent) {
            var streams = [];
            streams.push(streamEvent.stream);
            subscribeToStreams(streams);
            document.getElementById("recordButton").disabled = false;
          });

          room.addEventListener("stream-removed", function (streamEvent) {
            // Remove stream from DOM
            var stream = streamEvent.stream;
            if (stream.elementID !== undefined) {
              var element = document.getElementById(stream.elementID);
              document.body.removeChild(element);
            }
          });
          
          room.addEventListener("stream-failed", function (streamEvent){
              console.log("STREAM FAILED, DISCONNECTION");
              room.disconnect();

          });

          room.connect();

          localStream.show("myVideo");

        });
        localStream.init();
      });
    }
};

app.initialize();

