
// This server should be updated
var serverUrl = "http://192.168.18.58:3001/";

var localStream, room, recording, recordingId;

function connectServer(id) {
    var txtbox = document.getElementById(id);
    serverUrl = txtbox.value;
    connect_function();
}

function disconnectServer() {
    localStream.stop();
}

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
      results = regex.exec(location.search);
  return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function testConnection () {
  window.location = "/connection_test.html";
}

function startRecording () {
  if (room !== undefined){
    if (!recording){
      room.startRecording(localStream, function(id) {
        recording = true;
        recordingId = id;
      });
    } 
  }
}

function stopRecording() {
  if (room !== undefined){
    room.stopRecording(recordingId);
    recording = false;
  }
}

function playRecordedFile() {
    // To play file via MCU Server
    // Following codes are example
    /*
    var stream1 = Erizo.Stream({video: true, audio: false, url:"file:///path_to_file/previousRecording.mkv"});
    room.publish(stream1);
    var stream2 = Erizo.Stream({audio:true, video:true, recording: 'asda2131231'});
    room.publish(stream2); 
    */
}

//window.onload = function () {
function connect_function () {

  /* TODO: Need to communicate with MCU Servers, to decide the serverUrl */
  /*       Or check some configuration file contains the list of MCU Servers. */

  recording = false;
  var screen = getParameterByName("screen");
  //var config = {audio: true, video: true, data: true, screen: screen, videoSize: [640, 480, 640, 480]};
  //var config = {audio: true, video: false, data: false, screen: false};
  var config = {audio: true, video: false, data: false, screen: screen, videoSize: [640, 480, 640, 480]};
  // If we want screen sharing we have to put our Chrome extension id. The default one only works in our Lynckia test servers.
  // If we are not using chrome, the creation of the stream will fail regardless.
  if (screen){
    config.extensionId = "okeephmleflklcdebijnponpabbmmgeo";
  }
  localStream = Erizo.Stream(config);
  var createToken = function(userName, role, callback) {

    var req = new XMLHttpRequest();
    var url = serverUrl + 'createToken/';
    var body = {username: userName, role: role};

    req.onreadystatechange = function () {
      if (req.readyState === 4) {
        callback(req.responseText);
      }
    };

    req.open('POST', url, true);
    req.setRequestHeader('Content-Type', 'application/json');
    req.send(JSON.stringify(body));
  };

  createToken("user", "presenter", function (response) {
    var token = response;
    console.log(token);
    room = Erizo.Room({token: token});

    localStream.addEventListener("access-accepted", function () {
      var subscribeToStreams = function (streams) {
        for (var index in streams) {
          var stream = streams[index];
          if (localStream.getID() !== stream.getID()) {
            room.subscribe(stream);
          }
        }
      };

      room.addEventListener("room-connected", function (roomEvent) {

        room.publish(localStream, {maxVideoBW: 300});
        subscribeToStreams(roomEvent.streams);
      });

      room.addEventListener("stream-subscribed", function(streamEvent) {
        var stream = streamEvent.stream;
        var div = document.createElement('div');
        div.setAttribute("style", "width: 320px; height: 240px;");
        div.setAttribute("id", "test" + stream.getID());

        document.body.appendChild(div);
        stream.show("test" + stream.getID());

      });

      room.addEventListener("stream-added", function (streamEvent) {
        var streams = [];
        streams.push(streamEvent.stream);
        subscribeToStreams(streams);
        document.getElementById("recordButton").disabled = false;
      });

      room.addEventListener("stream-removed", function (streamEvent) {
        // Remove stream from DOM
        var stream = streamEvent.stream;
        if (stream.elementID !== undefined) {
          var element = document.getElementById(stream.elementID);
          document.body.removeChild(element);
        }
      });
      
      room.addEventListener("stream-failed", function (streamEvent){
          console.log("STREAM FAILED, DISCONNECTION");
          room.disconnect();

      });

      room.connect();

      localStream.show("myVideo");

    });
    localStream.init();
  });
};
